cmake_minimum_required(VERSION 3.20)
project(debugging)

set(CMAKE_CXX_STANDARD 14)

include(cmake/Sanitizers.cmake)

add_executable(step_1 main.cpp)

add_executable(step_2 main.cpp)
target_link_libraries(step_2 sanitizers)

add_executable(step_3 main-safer.cpp)
target_link_libraries(step_3 sanitizers)
