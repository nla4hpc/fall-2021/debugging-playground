#include <iostream>
#include <memory>
#include <vector>


void use_after_free(std::shared_ptr<int> p){
  std::cout << *p + 3 << std::endl;
}

void consume(std::unique_ptr<int> p){
  std::cout << *p * *p << std::endl;
}

void consume(std::shared_ptr<int> p){
  std::cout << *p * *p << std::endl;
}

int oob_read(const std::vector<int>& arr){
  int sum = 0;
  for (int i : arr) {
    sum += i;
  }
  return sum;
}

void oob_write(std::vector<int>& arr, const int val){
  for (int & i : arr) {
    i = val;
  }
}

std::unique_ptr<int> local_reference(){
  int val = 13;
  return std::make_unique<int>(val);
}

struct bad_struct{
  int val;
};

bad_struct local_reference_struct(){
  int val = 13;
  bad_struct s{val};
  return s;
}


int main(int argc, char** argv) {
  int section = argc == 2 ? std::stoi(argv[1]) : 0;
  // double free
  if (!section || section == 1)
  {
    auto double_free = std::make_unique<int>(6);
    consume(std::move(double_free));
  }

  // use after free
  if (!section || section == 2)
  {
    auto freed = std::make_shared<int>(7);
    consume(freed);
    use_after_free(freed);
  }

  // missing free
  if (!section || section == 3)
  {
    auto not_freed = std::make_shared<int>(8);
    use_after_free(not_freed);
  }

  // use of uninitialized memory
  if (!section || section == 4)
  {
    std::vector<int> arr(10, 0);
    std::cout << oob_read(arr) << std::endl;
  }

  // out-of-bounds read
  if (!section || section == 5)
  {
    std::vector<int> arr(10, 0);
    oob_write(arr, 1);
    std::cout << oob_read(arr) << std::endl;
  }

  // out-of-bounds write
  if (!section || section == 6)
  {
    std::vector<int> arr(10, 2);
    oob_write(arr, 0);
    std::cout << oob_read(arr) << std::endl;
  }

  // reference to local variable
  if (!section || section == 7)
  {
    auto p = local_reference();
    std::cout << *p << std::endl;
  }

  // reference to local variable within struct
  if (!section || section == 8)
  {
    auto s = local_reference_struct();
    std::cout << s.val << std::endl;
  }
}
